package tech.triumphit.alumni.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Tushar on 11/21/2016.
 */

public class SharedPreferencePrivate {
    public static SharedPreferences sp;
    public static SharedPreferences.Editor editor;

    public SharedPreferencePrivate(Context context){
        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();
    }
}
