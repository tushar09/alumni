package tech.triumphit.alumni.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Random;

import tech.triumphit.alumni.R;
import tech.triumphit.alumni.databinding.NotiRowBinding;

/**
 * Created by Tushar on 11/3/2016.
 */

public class NotificationListAdapter extends BaseAdapter {

    ArrayList<String> name, email, noti_body, date;
    ArrayList<Integer> status;
    Context context;

    int ring [];

    public NotificationListAdapter(Context context, ArrayList<String> name, ArrayList<String> email, ArrayList<String> noti_body, ArrayList<String> date, ArrayList<Integer> status){
        this.context = context;
        this.name = name;
        this.email = email;
        this.noti_body = noti_body;
        this.date = date;
        this.name = name;
        this.status = status;

        ring = new int[5];
        ring[0] = R.drawable.ring;
        ring[1] = R.drawable.ring_two;
        ring[2] = R.drawable.ring_three;
        ring[3] = R.drawable.ring_four;
        ring[4] = R.drawable.ring_five;
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = holder.binding.getRoot();
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }

        holder.binding.body.setText(noti_body.get(position));
        holder.binding.date.setText(date.get(position));
        holder.binding.ring.setImageResource(ring[new Random().nextInt(ring.length)]);
        Glide.with(context).load("http://triumphit.tech/project_alumni/images/" + email.get(position) + ".jpeg").into(holder.binding.profileImage);
        if(status.get(position) == 1){
            holder.binding.body.setTypeface(Typeface.DEFAULT_BOLD);
        }else {
            holder.binding.body.setTypeface(holder.binding.body.getTypeface(), Typeface.NORMAL);
        }
        Log.e("image", "http://triumphit.tech/project_alumni/images/" + email.get(position));
        return convertView;
    }

    class Holder{
        NotiRowBinding binding;
        Holder(){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.noti_row, null, true);
        }
    }
}
