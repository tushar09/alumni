package tech.triumphit.alumni.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import tech.triumphit.alumni.*;
import tech.triumphit.alumni.Chat;
import tech.triumphit.alumni.databinding.OnlineRowBinding;

/**
 * Created by Tushar on 11/6/2016.
 */

public class OnlineListAdapter extends BaseAdapter {

    ArrayList status;
    ArrayList name, email, alumni, propic;

    Context context;

    public OnlineListAdapter(Context context, ArrayList status, ArrayList name, ArrayList email, ArrayList propic, ArrayList alumni){
        this.context = context;
        this.name = name;
        this.email = email;
        this.alumni = alumni;
        this.propic = propic;
        this.status = status;
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = holder.binding.getRoot();
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }

        Glide.with(context).load("http://triumphit.tech/project_alumni/images/" + email.get(position) + ".jpeg").into(holder.binding.profileImage);
        holder.binding.name.setText("" + name.get(position));
        holder.binding.bio.setText("" + alumni.get(position));
        if(Integer.parseInt("" + status.get(position)) == 1){
            holder.binding.status.setImageResource(R.drawable.online);
        }else{
            holder.binding.status.setImageResource(R.drawable.offline);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, Chat.class).putExtra("toEmail", "" + email.get(position)));
            }
        });

        return convertView;
    }

    private class Holder{
        OnlineRowBinding binding;
        Holder(){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.online_row, null, true);
        }
    }
}
