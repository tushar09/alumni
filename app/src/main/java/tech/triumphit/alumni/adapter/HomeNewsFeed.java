package tech.triumphit.alumni.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import tech.triumphit.alumni.Home;
import tech.triumphit.alumni.R;
import tech.triumphit.alumni.databinding.RowFeedsBinding;

/**
 * Created by Tushar on 10/10/2016.
 */

public class HomeNewsFeed extends BaseAdapter {

    ArrayList name, date, propic, postPic, postText, totalLike, likerProPic, feedsID, likedDate, id;
    ArrayList like;
    Context context;
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    public HomeNewsFeed(Context context, ArrayList name, ArrayList postPic, ArrayList date, ArrayList propic, ArrayList postText, ArrayList totalLike, ArrayList likerProPic, ArrayList feedsID, ArrayList likedDate, ArrayList id){
        this.context = context;
        this.name = name;
        this.date = date;
        this.propic = propic;
        this.postPic = postPic;
        this.postText = postText;
        this.totalLike = totalLike;
        this.likerProPic = likerProPic;
        this.feedsID = feedsID;
        this.likedDate = likedDate;

        like = new ArrayList();

        this.id = id;

        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = holder.rowFeedsBinding.getRoot();
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }
        holder.rowFeedsBinding.textView23.setText("" + name.get(position));
        holder.rowFeedsBinding.textView25.setText("" + date.get(position));
        holder.rowFeedsBinding.textView26.setText("" + postText.get(position));
        holder.rowFeedsBinding.textView27.setText("" + totalLike.get(position));
        Glide.with(context).load("" + propic.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.rowFeedsBinding.profileImage);
        Log.e("propic", "" + propic.get(position));

        if(("" + id.get(position)).equals(("" + feedsID.get(position)))){
            //Glide.with(context).load(R.drawable.heart_liked).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.rowFeedsBinding.imageButton);
            holder.rowFeedsBinding.imageButton.setBackgroundResource(R.drawable.heart_liked);
        }else{
            //Glide.with(context).load(R.drawable.heart).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.rowFeedsBinding.imageButton);
            holder.rowFeedsBinding.imageButton.setBackgroundResource(R.drawable.heart);
        }

        Glide.with(context).load("" + likerProPic.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().placeholder(R.drawable.placeholder).into(holder.rowFeedsBinding.profileImage2);
        Glide.with(context).load("" + postPic.get(position)).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.rowFeedsBinding.imageView.setBackgroundColor(Color.BLACK);
                holder.rowFeedsBinding.textView26.setTextColor(Color.parseColor("#ffffff"));
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                return false;
            }
        }).into(holder.rowFeedsBinding.imageView);

        holder.rowFeedsBinding.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.rowFeedsBinding.loadingView.smoothToShow();
                StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/setLike.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                holder.rowFeedsBinding.loadingView.smoothToHide();
                                Animation expandIn = AnimationUtils.loadAnimation(context, R.anim.expand_in);
                                if(response.equals("0")){
                                    feedsID.set(position, "null");
                                    totalLike.set(position, "" + (Integer.parseInt("" + totalLike.get(position)) - 1));
                                    holder.rowFeedsBinding.textView27.setText("" + totalLike.get(position));
                                    holder.rowFeedsBinding.imageButton.setBackgroundResource(R.drawable.heart);
                                    likerProPic.set(position, "");
                                    Glide.with(context).load("" + likerProPic.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL).animate(R.anim.expand_in).centerCrop().placeholder(R.drawable.placeholder).into(holder.rowFeedsBinding.profileImage2);
                                    holder.rowFeedsBinding.textView27.startAnimation(expandIn);
                                    holder.rowFeedsBinding.imageButton.startAnimation(expandIn);
                                }else{
                                    feedsID.set(position, ("" + id.get(position)));
                                    totalLike.set(position, "" + (Integer.parseInt("" + totalLike.get(position)) + 1));
                                    likerProPic.set(position, "http://triumphit.tech/project_alumni/images/" + sp.getString("email", "non") + ".jpeg");
                                    holder.rowFeedsBinding.textView27.setText("" + totalLike.get(position));
                                    holder.rowFeedsBinding.imageButton.setBackgroundResource(R.drawable.heart_liked);
                                    Glide.with(context).load("" + likerProPic.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().placeholder(R.drawable.placeholder).into(holder.rowFeedsBinding.profileImage2);
                                    holder.rowFeedsBinding.textView27.startAnimation(expandIn);
                                    holder.rowFeedsBinding.imageButton.startAnimation(expandIn);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                NetworkResponse networkResponse = error.networkResponse;
                                if (networkResponse != null) {
                                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                }

                                if (error instanceof TimeoutError) {
                                    Log.e("Volley", "TimeoutError " + error.toString());
                                } else if (error instanceof NoConnectionError) {
                                    Log.e("Volley", "NoConnectionError");
                                } else if (error instanceof AuthFailureError) {
                                    Log.e("Volley", "AuthFailureError");
                                } else if (error instanceof ServerError) {
                                    Log.e("Volley", "ServerError");
                                } else if (error instanceof NetworkError) {
                                    Log.e("Volley", "NetworkError");
                                } else if (error instanceof ParseError) {
                                    Log.e("Volley", "ParseError");
                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
                        String currentDateandTime = sdf.format(new Date());
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", sp.getString("email", "non"));
                        params.put("id", "" + id.get(position));
                        params.put("date", "" + currentDateandTime);
                        return params;
                    }
                };
                sr.setRetryPolicy(new DefaultRetryPolicy(
                        180000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(sr);
            }
        });

        return convertView;
    }

    class Holder{
        RowFeedsBinding rowFeedsBinding;
        Holder(){
            rowFeedsBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_feeds, null, true);
        }
    }
}
