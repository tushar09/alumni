package tech.triumphit.alumni.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import tech.triumphit.alumni.R;
import tech.triumphit.alumni.databinding.RowChatFriendBinding;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Tushar on 11/15/2016.
 */

public class Chat extends BaseAdapter {

    private final SharedPreferences sp;
    private final SharedPreferences.Editor editor;
    Context context;
    ArrayList id, msg, email, propic;

    public Chat(Context context, ArrayList id, ArrayList msg, ArrayList email, ArrayList propic){
        this.context = context;
        this.id = id;
        this.msg = msg;
        this.email = email;
        this.propic = propic;

        sp = context.getSharedPreferences("utils", MODE_PRIVATE);
        editor = sp.edit();

    }

    @Override
    public int getCount() {
        return msg.size();
    }

    @Override
    public Object getItem(int position) {
        return msg.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if(convertView == null){
            holder = new Holder();
            convertView = holder.rowChatFriendBinding.getRoot();
            convertView.setTag(holder);

        }else{
            holder = (Holder) convertView.getTag();
        }

        holder.rowChatFriendBinding.body.setText("" + msg.get(position));

        if(("" + email.get(position)).equals(sp.getString("email", "asdf"))){
            holder.rowChatFriendBinding.profileImage2.setVisibility(View.GONE);
            holder.rowChatFriendBinding.profileImage.setVisibility(View.VISIBLE);
            holder.rowChatFriendBinding.msgContainer.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            Glide.with(context).load("" + propic.get(position)).centerCrop().into(holder.rowChatFriendBinding.profileImage);
        }else {
            holder.rowChatFriendBinding.profileImage.setVisibility(View.GONE);
            holder.rowChatFriendBinding.profileImage2.setVisibility(View.VISIBLE);
            holder.rowChatFriendBinding.msgContainer.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            Glide.with(context).load("" + propic.get(position)).centerCrop().into(holder.rowChatFriendBinding.profileImage2);
        }



        return convertView;
    }

    class Holder{

        RowChatFriendBinding rowChatFriendBinding;

        Holder(){
            rowChatFriendBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_chat_friend, null, true);
        }
    }

}
