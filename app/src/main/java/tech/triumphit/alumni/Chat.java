package tech.triumphit.alumni;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.BaseAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tech.triumphit.alumni.databinding.ActivityChatBinding;
import tech.triumphit.alumni.events.EventsInterfaces;
import tech.triumphit.alumni.notification.GCMPushReceiverService;

public class Chat extends AppCompatActivity implements EventsInterfaces.OnReceiveMessage{

    ActivityChatBinding binding;

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    String toEmail;

    public static ArrayList id, msg, email, propic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        GCMPushReceiverService.initChat(this);

        sp = getSharedPreferences("utils", MODE_PRIVATE);
        editor = sp.edit();

        toEmail = getIntent().getStringExtra("toEmail");
        Log.e("to emial", toEmail);

        getMessages(toEmail);

        binding.content.editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //Toast.makeText(HelloFormStuff.this, edittext.getText(), Toast.LENGTH_SHORT).show();
                    sendMessage(binding.content.editText.getText().toString(), toEmail);
                    return true;
                }
                return false;
            }
        });

        binding.content.editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (binding.content.editText.getRight() - binding.content.editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        sendMessage(binding.content.editText.getText().toString(), toEmail);

                        return true;
                    }
                }
                return false;
            }
        });

        binding.content.sendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.content.editText.getText().toString().equals("")){
                    YoYo.with(Techniques.Shake).duration(1000).playOn(binding.content.editText);
                }else {
                    YoYo.with(Techniques.Tada).duration(1000).playOn(v);
                    YoYo.with(Techniques.FadeOut).withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            binding.content.editText.setText("");
                            YoYo.with(Techniques.FadeIn).duration(1000).playOn(binding.content.editText);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    }).duration(1000).playOn(binding.content.editText);


                }

            }
        });

    }

    private void sendMessage(final String msg, final String toEmail) {
        binding.content.editText.setText("");
        id.add(Integer.parseInt("" + id.get(id.size() - 1)));
        this.msg.add(msg);
        email.add(sp.getString("email", "non"));
        propic.add("http://triumphit.tech/project_alumni/images/" + sp.getString("email", "non") + ".jpeg");
        ((BaseAdapter) binding.content.lv.getAdapter()).notifyDataSetChanged();
        StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/setChat.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("error", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }

                        if (error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError " + error.toString());
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("emailFrom", sp.getString("email", "non"));
                params.put("emailTo", toEmail);
                params.put("msg", msg);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(Chat.this);
        requestQueue.add(sr);
    }

    public void getMessages(final String toEmail) {

        id = new ArrayList();
        msg = new ArrayList();
        email = new ArrayList();
        propic = new ArrayList();

        StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/getChat.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("error", response);
                        JSONObject jo;
                        try {
                            jo = new JSONObject(response);
                            JSONArray jr = jo.getJSONArray("FullData");
                            Log.e("error", response);
                            for (int t = 0; t < jr.length(); t++) {
                                JSONObject jsonObject = jr.getJSONObject(t);
                                id.add("" + jsonObject.getString("id"));
                                msg.add("" + jsonObject.getString("msg"));
                                email.add(jsonObject.getString("email"));
                                propic.add(jsonObject.getString("propic"));
                                //Log.e("error", jsonObject.getString("feedsID"));
                            }

                            binding.content.lv.setAdapter(new tech.triumphit.alumni.adapter.Chat(Chat.this, id, msg, email, propic));
                            //binding.swipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e) {
                            Log.e("errorasdfasdf", e.toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }

                        if (error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError " + error.toString());
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("emailFrom", sp.getString("email", "non"));

                params.put("emailTo", toEmail);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(Chat.this);
        requestQueue.add(sr);
    }


    @Override
    public void OnReceive(String message, String email) {
        id.add("34");
        msg.add(message);
        Log.e("message from chat", message);
        this.email.add(email);
        propic.add("http://triumphit.tech/project_alumni/images/" + email + ".jpeg");

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((tech.triumphit.alumni.adapter.Chat) binding.content.lv.getAdapter()).notifyDataSetChanged();
            }
        });

//        Handler refresh = new Handler(Looper.getMainLooper());
//        refresh.post(new Runnable() {
//            public void run() {
//
//            }
//        });

    }
}
