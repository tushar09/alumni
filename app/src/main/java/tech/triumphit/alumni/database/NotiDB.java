package tech.triumphit.alumni.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Tushar on 11/5/2016.
 */

public class NotiDB extends SQLiteOpenHelper {

    public static final String DB_NAME = "notiDB.db";


    public NotiDB(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table noti " +
                        "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name text,body text,email text, date text, readStatus int)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long insertRow(String name, String body, String email, String date){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);
        contentValues.put("body", body);
        contentValues.put("email", email);
        contentValues.put("date", date);
        contentValues.put("readStatus", 1);

        return db.insert("noti", null, contentValues);
    }

    public ArrayList [] getData(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from noti order by id desc", null);

        ArrayList<String> email = new ArrayList<String>();
        ArrayList<String> body = new ArrayList<String>();
        ArrayList<String> date = new ArrayList<String>();
        ArrayList<String> name = new ArrayList<String>();
        ArrayList<Integer> status = new ArrayList<Integer>();

        Log.e("cursor size", "" + cursor.getCount());
        if(cursor.moveToFirst()){

            do{
                email.add(cursor.getString(cursor.getColumnIndex("email")));
                body.add(cursor.getString(cursor.getColumnIndex("body")));
                name.add(cursor.getString(cursor.getColumnIndex("name")));
                date.add(getTimeFormat(cursor.getString(cursor.getColumnIndex("date"))));
                status.add(cursor.getInt(cursor.getColumnIndex("readStatus")));
            }while(cursor.moveToNext());
        }

        cursor.close();
        db.close();

        ArrayList [] result = new ArrayList[5];
        result[0] = email;
        result[1] = body;
        result[2] = date;
        result[3] = name;
        result[4] = status;

        return result;

    }

    public String getTimeFormat(String duration){
        long time = Long.parseLong(duration);
        long currentTime = new Date().getTime();

        long finalDuration = currentTime - time;

        long seconds = finalDuration / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        if(days != 0){
            return days + " days ago.";
        }else if(hours != 0){
            return hours + " hours ago.";
        }else if(minutes != 0){
            return minutes + " minutes ago.";
        }else{
            return " just now";
        }
    }


}
