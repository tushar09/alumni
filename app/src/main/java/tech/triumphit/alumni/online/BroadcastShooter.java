package tech.triumphit.alumni.online;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.LogManager;

import tech.triumphit.alumni.Home;
import tech.triumphit.alumni.adapter.HomeNewsFeed;

/**
 * Created by Tushar on 11/6/2016.
 */

public class BroadcastShooter {

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    Context context;

    public BroadcastShooter(Context context){
        Foreground.get(context).addListener(listener);
        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();
        this.context = context;
    }

    Foreground.Listener listener = new Foreground.Listener() {
        @Override
        public void onBecameForeground() {
            Log.e("online", "online");
            sendRequest("1");
        }

        @Override
        public void onBecameBackground() {
            Log.e("offline", "offline");
            sendRequest("0");
        }
    };

    private void sendRequest(final String status){
        StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/setOnlineStatus.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }

                        if (error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError " + error.toString());
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", sp.getString("email", "non"));
                params.put("status", status);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(sr);
    }

}
