package tech.triumphit.alumni.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import tech.triumphit.alumni.Home;
import tech.triumphit.alumni.R;
import tech.triumphit.alumni.database.NotiDB;
import tech.triumphit.alumni.events.EventShooter;
import tech.triumphit.alumni.events.EventsInterfaces;

/**
 * Created by NgocTri on 4/9/2016.
 */
public class GCMPushReceiverService extends GcmListenerService {

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    NotiDB notiDB;

    static Context chat, home;

    public static void initHome(Context context){
        GCMPushReceiverService.home = context;
    }

    public static void initChat(Context context){
        GCMPushReceiverService.chat = context;
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();
        EventShooter shooter = new EventShooter();

        if(data.getString("message").contains("sent a message to you")){
            String message = data.getString("message");
            String name = data.getString("name");
            String email = data.getString("email");

            message = message.replace(name + " sent a message to you: ", "");

            Log.e("message", "onMessageReceived: " + message);

            shooter.prepareMessage((EventsInterfaces.OnReceiveMessage) chat);
            shooter.shootMessage(message, email);

        }else{
            shooter.prepareNotification((EventsInterfaces.OnReceiveNotification) home);
            shooter.shootNotification(sp.getInt("noti_count", 1));
            String message = data.getString("message");
            String name = data.getString("name");
            String email = data.getString("email");

            //sendNotification(message, name);
            MyAsync myAsync = new MyAsync();
            myAsync.execute(email, name, message);
        }





    }

    private void sendNotification(String message, String name, String email, Bitmap b) {
        editor.putInt("noti_count", sp.getInt("noti_count", 0) + 1);
        editor.commit();

        notiDB = new NotiDB(this);
        long g = notiDB.insertRow(name, message, email, "" + new Date().getTime());

        if(g == -1){

        }else{
            Log.e("insert ok", "" + g);
        }

        Intent intent = new Intent(this, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0;//Your request code
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        //Setup notification
        //Sound
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //Build notification
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(b)
                .setTicker(message)
                .setContentTitle(name)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, noBuilder.build()); //0 = ID of notification
    }

    public class MyAsync extends AsyncTask<String, String, String> {
        Bitmap myBitmap = null;
        String message = "";
        String name = "";
        String email = "http://triumphit.tech/project_alumni/images/";
        String emailExact = "";

        @Override
        protected String doInBackground(String... params) {
            try {
                message = params[2];
                name = params[1];
                email += params[0] + ".jpeg";
                emailExact = params[0];
                URL url = new URL(email);
                Log.e("email not", email);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                myBitmap = BitmapFactory.decodeStream(input);
                Resources res = GCMPushReceiverService.this.getResources();
                int height = (int) res.getDimension(android.R.dimen.notification_large_icon_height);
                int width = (int) res.getDimension(android.R.dimen.notification_large_icon_width);
                myBitmap = Bitmap.createScaledBitmap(myBitmap, height, height, false);
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("error not", e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            sendNotification(message, name, emailExact, myBitmap);
            Log.e("came here", myBitmap.toString());
        }

    }

    public static float getImageFactor(Resources r) {
        DisplayMetrics metrics = r.getDisplayMetrics();
        float multiplier = metrics.density / 3f;
        return multiplier;
    }
}
