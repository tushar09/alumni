package tech.triumphit.alumni;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.antlr.v4.codegen.model.SrcOp;

import java.util.ArrayList;

import tech.triumphit.alumni.adapter.NotificationListAdapter;
import tech.triumphit.alumni.database.NotiDB;
import tech.triumphit.alumni.databinding.ActivityNotificationBinding;
import tech.triumphit.alumni.events.EventShooter;

public class Notification extends AppCompatActivity {

    ActivityNotificationBinding binding;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    NotiDB notiDB;
    ArrayList [] result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        setSupportActionBar(binding.toolbar);

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        editor.putInt("noti_count", 0);
        editor.commit();

        notiDB = new NotiDB(this);

        result = notiDB.getData();

        ArrayList<String> email = result[0];
        ArrayList<String> body = result[1];
        ArrayList<String> date = result[2];
        ArrayList<String> name = result[3];
        ArrayList<Integer> status = result[4];

        Log.e("size", "" + name.size());
        for(int t = 0; t < name.size(); t++){
            Log.e("name", name.get(t));
        }

        binding.content.lv.setAdapter(new NotificationListAdapter(this, name, email, body, date, status));

    }

}
