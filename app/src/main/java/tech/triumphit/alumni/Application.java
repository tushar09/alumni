package tech.triumphit.alumni;

import android.content.ComponentCallbacks2;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import tech.triumphit.alumni.online.Foreground;

/**
 * Created by Tushar on 10/25/2016.
 */

public class Application extends android.app.Application {


    private SharedPreferences sp;

    @Override
    public void onCreate() {
        super.onCreate();

        Foreground.init(this);
        sp = getSharedPreferences("utils", MODE_PRIVATE);
        Log.e("wer","wer");
        StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/setOnlineStatus.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }

                        if (error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError " + error.toString());
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", sp.getString("email", "non"));
                params.put("status", "1");
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(sr);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        //
        //
        // Log.e("ter","ter");
        //Toast.makeText(this, "asdfasdf", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            Log.e("ter","ter");
            //Toast.makeText(this, "TRIM_MEMORY_UI_HIDDEN", Toast.LENGTH_SHORT).show();
        }else if(level == ComponentCallbacks2.TRIM_MEMORY_BACKGROUND){
            Log.e("ter","TRIM_MEMORY_BACKGROUND)");
            //Toast.makeText(this, "TRIM_MEMORY_BACKGROUND", Toast.LENGTH_SHORT).show();
        }else if(level == ComponentCallbacks2.TRIM_MEMORY_COMPLETE){
            Log.e("ter","TRIM_MEMORY_COMPLETE");
            //Toast.makeText(this, "TRIM_MEMORY_COMPLETE", Toast.LENGTH_SHORT).show();
        }else if(level == ComponentCallbacks2.TRIM_MEMORY_MODERATE){
            Log.e("ter","TRIM_MEMORY_MODERATE");
            //Toast.makeText(this, "TRIM_MEMORY_MODERATE", Toast.LENGTH_SHORT).show();
        }else if(level == ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL){
            Log.e("ter","TRIM_MEMORY_RUNNING_CRITICAL");
            //Toast.makeText(this, "TRIM_MEMORY_RUNNING_CRITICAL", Toast.LENGTH_SHORT).show();
        }else if(level == ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW){
            Log.e("ter","TRIM_MEMORY_RUNNING_LOW");
            //Toast.makeText(this, "TRIM_MEMORY_RUNNING_LOW", Toast.LENGTH_SHORT).show();
        }else if(level == ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE){
            Log.e("ter","TRIM_MEMORY_RUNNING_MODERATE");
            //Toast.makeText(this, "TRIM_MEMORY_RUNNING_MODERATE", Toast.LENGTH_SHORT).show();
        }

    }

}
