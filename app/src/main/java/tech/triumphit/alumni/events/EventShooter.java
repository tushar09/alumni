package tech.triumphit.alumni.events;

import android.util.Log;

/**
 * Created by Tushar on 11/3/2016.
 */

public class EventShooter {

    EventsInterfaces.OnReceiveNotification onReceiveNotification;
    EventsInterfaces.OnReceiveMessage onReceiveMessage;

    public void prepareNotification(EventsInterfaces.OnReceiveNotification onReceiveNotification){
        this.onReceiveNotification = onReceiveNotification;
    }

    public void prepareMessage(EventsInterfaces.OnReceiveMessage onReceiveMessage){
        this.onReceiveMessage = onReceiveMessage;
    }

    public void shootNotification(int count){
        try{
            onReceiveNotification.OnReceive(count);

        }catch (NullPointerException e){
            Log.e("error event", e.toString());
        }
    }

    public void shootMessage(String msg, String email){
        try {
            onReceiveMessage.OnReceive(msg, email);
        }catch (NullPointerException e ){

        }
    }

}
