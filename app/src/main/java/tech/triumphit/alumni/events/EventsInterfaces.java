package tech.triumphit.alumni.events;

/**
 * Created by Tushar on 11/3/2016.
 */

public class EventsInterfaces {
    public interface OnReceiveNotification{
        void OnReceive(int count);
    }

    public interface OnReceiveMessage{
        void OnReceive(String message, String email);
    }
}
