package tech.triumphit.alumni;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import tech.triumphit.alumni.adapter.HomeNewsFeed;
import tech.triumphit.alumni.adapter.OnlineListAdapter;
import tech.triumphit.alumni.databinding.DrawerHolderBinding;
import tech.triumphit.alumni.databinding.NavDrawerHeaderBinding;
import tech.triumphit.alumni.events.EventsInterfaces;
import tech.triumphit.alumni.notification.GCMPushReceiverService;
import tech.triumphit.alumni.notification.GCMRegistrationIntentService;
import tech.triumphit.alumni.online.BroadcastShooter;

public class Home extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, EventsInterfaces.OnReceiveNotification {

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    DrawerHolderBinding binding;
    NavDrawerHeaderBinding navDrawerHeaderBinding;
    private AlertDialog pd;
    Animation animDown;
    private Animation animDownPostButon, animDownPost;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    static int mNotifCount = 0;
    static RelativeLayout notifCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.drawer_holder);
        setSupportActionBar(binding.toolbar);


        sp = getSharedPreferences("utils", MODE_PRIVATE);
        editor = sp.edit();

        GCMPushReceiverService.initHome(this);
        BroadcastShooter bs = new BroadcastShooter(this);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)){
                    //Registration success
                    String token = intent.getStringExtra("token");

                } else if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)){
                    //Registration error

                } else {
                    //Tobe define
                }
            }
        };
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if(ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                //SoF notification
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }

//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setTitle("sdaf");
//        progressDialog.setMessage("Authenticating...");
//        progressDialog.show();


        //String url = "https://api.linkedin.com/v1/people/~:(email-address, formatted-name, phone-number, public-profile-url, picture-url, picture-urls::(original))";
        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,headline,picture-urls::(original),industry,summary,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),associations,interests,num-recommenders,date-of-birth,publications:(id,title,publisher:(name),authors:(id,name),date,url,summary),patents:(id,title,summary,number,status:(id,name),office:(name),inventors:(id,name),date,url),languages:(id,language:(name),proficiency:(level,name)),skills:(id,skill:(name)),certifications:(id,name,authority:(name),number,start-date,end-date),courses:(id,name,number),recommendations-received:(id,recommendation-type,recommendation-text,recommender),honors-awards,three-current-positions,three-past-positions,volunteer)";

        final ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.app_name,
                R.string.app_name);
        binding.drawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        navDrawerHeaderBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.nav_drawer_header, binding.shitstuff, true);
        navDrawerHeaderBinding.textView12.setText(sp.getString("name", "Not Found"));
        Picasso.with(this).load(new File(sp.getString("pic", ""))).into(navDrawerHeaderBinding.profileImage, new Callback() {
            @Override
            public void onSuccess() {
                Log.e("From file", "Success");
            }

            @Override
            public void onError() {
                Picasso.with(Home.this).load("http://triumphit.tech/project_alumni/images/" + sp.getString("email", "") + ".jpeg").into(navDrawerHeaderBinding.profileImage);
                Log.e("From file", "Not Success " + "http://triumphit.tech/project_alumni/images/" + sp.getString("email", "") + ".jpeg");
            }
        });
        binding.shitstuff.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                if (item.getItemId() == R.id.update) {
                    startActivity(new Intent(Home.this, UpdateProfile.class));
                }
                if (item.getItemId() == R.id.settings) {
                    startActivity(new Intent(Home.this, Profile.class));
                }
                return false;
            }
        });

        binding.postPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Home.this)
                        .setTitle("Photo")
                        .setMessage("Take Photo from camera or folder")
                        .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                                startActivityForResult(intent, 420);
                            }
                        })
                        .setNegativeButton("Folder", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<Image> images = new ArrayList<Image>();
                                ImagePicker.create(Home.this)
                                        .folderMode(true) // folder mode (false by default)
                                        .folderTitle("Folder") // folder selection title
                                        .imageTitle("Tap to select") // image selection title
                                        .single() // single mode
                                        .multi() // multi mode (default mode)
                                        .limit(1) // max images can be selected (99 by default)
                                        .showCamera(false) // show camera or not (true by default)
                                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                                        .origin(images) // original selected images, used in multi mode
                                        .start(500); // start image picker activity with request code
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        binding.postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sp.getString("alumni", "non").equals("non")) {
                    SpannableStringBuilder builder = new SpannableStringBuilder();
                    builder.append("Update your profile first").append("  ");
                    builder.setSpan(new ImageSpan(Home.this, R.drawable.emoticon_sad), builder.length() - 1, builder.length(), 0);
                    Snackbar snackbar = Snackbar
                            .make(binding.postButton, builder, Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(Home.this, UpdateProfile.class));
                                }
                            });
                    snackbar.show();
                } else {
                    if (b == null && binding.post.getText().toString().equals("")) {
                        SpannableStringBuilder builder = new SpannableStringBuilder();
                        builder.append("Your thought is empty? How?").append("  ");
                        builder.setSpan(new ImageSpan(Home.this, R.drawable.emoticon_poop), builder.length() - 1, builder.length(), 0);
                        //builder.append(" next message");
                        Snackbar snackbar = Snackbar
                                .make(binding.postButton, builder, Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                });
                        snackbar.show();

                    } else {
                        StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/setFeeds.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("match", response);
                                        binding.loadingView.smoothToHide();
                                        animatePosterInvisible();
                                        anim = true;
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        NetworkResponse networkResponse = error.networkResponse;
                                        if (networkResponse != null) {
                                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                        }

                                        if (error instanceof TimeoutError) {
                                            Log.e("Volley", "TimeoutError " + error.toString());
                                        } else if (error instanceof NoConnectionError) {
                                            Log.e("Volley", "NoConnectionError");
                                        } else if (error instanceof AuthFailureError) {
                                            Log.e("Volley", "AuthFailureError");
                                        } else if (error instanceof ServerError) {
                                            Log.e("Volley", "ServerError");
                                        } else if (error instanceof NetworkError) {
                                            Log.e("Volley", "NetworkError");
                                        } else if (error instanceof ParseError) {
                                            Log.e("Volley", "ParseError");
                                        }
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
                                String currentDateandTime = sdf.format(new Date());
                                Log.e("date", currentDateandTime);
                                String postPic = "";
                                if (b != null) {
                                    //b = getResizedBitmap(b, 500);
                                    Log.e("here for b", currentDateandTime);
                                    postPic = getPropicBase64(b);
                                    Log.e("here for b base", postPic);
                                    b = null;
                                }
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("email", sp.getString("email", "non"));
                                params.put("alumni", sp.getString("alumni", "non"));
                                params.put("date", currentDateandTime);
                                if (postPic.equals("")) {
                                    params.put("postpic", "not");
                                    Log.e("postpic", "postPic null");
                                } else {
                                    params.put("postpic", postPic);
                                }
                                params.put("posttext", binding.post.getText().toString());

                                Log.e("email", sp.getString("email", "asdf"));
                                return params;
                            }
                        };
                        sr.setRetryPolicy(new DefaultRetryPolicy(
                                180000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(Home.this);
                        requestQueue.add(sr);
                        binding.loadingView.smoothToShow();
                    }
                }

            }
        });

        binding.lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.e("SCROLLING DOWN", "TRUE");
                    //animatePosterInvisible();
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    Log.e("SCROLLING UP", "TRUE");
                    //animatePosterVisible();
                }
                mLastFirstVisibleItem = firstVisibleItem;

            }
        });

        binding.swipeRefreshLayout.setOnRefreshListener(this);
        binding.swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        binding.swipeRefreshLayout.setRefreshing(true);
                                        getFeed();
                                        getOnlineStatus();
                                    }
                                }
        );

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    }

    private void animatePosterVisible() {
        binding.ril.setVisibility(View.VISIBLE);
        animDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        animDownPost = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down_child);
        animDownPostButon = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down_child);
        animDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

                binding.post.startAnimation(animDownPost);
                binding.postButton.startAnimation(animDownPostButon);
                binding.postPic.startAnimation(animDownPostButon);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        binding.ril.startAnimation(animDown);
    }
    private void animatePosterInvisible() {
            animDown = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.slide_up);
            animDownPost = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.slide_up_child);
            animDownPostButon = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.slide_up_child);
            animDown.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    binding.post.startAnimation(animDownPost);
                    binding.postButton.startAnimation(animDownPostButon);
                    binding.postPic.startAnimation(animDownPostButon);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    //binding.ril.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            binding.ril.startAnimation(animDown);

    }

    Bitmap b = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 500 && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            try {
                b = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse("file://" + images.get(0).getPath()));
            } catch (IOException e) {
                Log.e("b", e.toString());
                Log.e("b path", images.get(0).getPath());
                b = null;
            }
            Glide.with(Home.this).load(images.get(0).getPath()).asBitmap().animate(R.anim.slide_in_right).diskCacheStrategy(DiskCacheStrategy.ALL).into(binding.imageView11);
        }
        if (requestCode == 420) {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            b = decodeSampledBitmapFromFile(file.getAbsolutePath(), 1000, 700);
            Bitmap bitmap = b;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            Glide.with(Home.this).load(byteArrayOutputStream.toByteArray()).asBitmap().animate(R.anim.slide_in_right).diskCacheStrategy(DiskCacheStrategy.ALL).into(binding.imageView11);
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    private String getPropicBase64(Bitmap b) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void getFeed() {

        if (sp.getString("alumni", "non").equals("non") || sp.getString("alumni", "non").equals("")) {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            builder.append("Update your profile first").append("  ");
            builder.setSpan(new ImageSpan(Home.this, R.drawable.emoticon_sad), builder.length() - 1, builder.length(), 0);
            Snackbar snackbar = Snackbar
                    .make(binding.postButton, builder, Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(Home.this, UpdateProfile.class));
                        }
                    });
            snackbar.show();
        } else {
            final ArrayList id, name, date, propic, postPic, postText, totalLike, likerProPic, feedsID, likedDate;

            id = new ArrayList();
            name = new ArrayList();
            date = new ArrayList();
            propic = new ArrayList();
            postPic = new ArrayList();
            postText = new ArrayList();
            totalLike = new ArrayList();
            likerProPic = new ArrayList();
            feedsID = new ArrayList();
            likedDate = new ArrayList();

            StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/getFeeds.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            JSONObject jo;
                            try {
                                jo = new JSONObject(response);
                                JSONArray jr = jo.getJSONArray("FullData");
                                //Log.e("error", jr.length() + "");
                                for (int t = 0; t < jr.length(); t++) {
                                    JSONObject jsonObject = jr.getJSONObject(t);
                                    id.add("" + jsonObject.getString("id"));
                                    name.add("" + jsonObject.getString("name"));
                                    date.add(jsonObject.getString("date"));
                                    propic.add("http://triumphit.tech/project_alumni/images/" + jsonObject.getString("email") + ".jpeg");
                                    postPic.add(jsonObject.getString("postpic"));
                                    postText.add(jsonObject.getString("posttext"));
                                    totalLike.add(jsonObject.getInt("totalLike"));
                                    likerProPic.add(jsonObject.getString("likerPropic"));
                                    feedsID.add(jsonObject.getString("feedsID"));
                                    likedDate.add(jsonObject.getString("likedDate"));
                                    Log.e("error", jsonObject.getString("feedsID"));
                                }

                                binding.lv.setAdapter(new HomeNewsFeed(Home.this, name, postPic, date, propic, postText, totalLike, likerProPic, feedsID, likedDate, id));
                                binding.swipeRefreshLayout.setRefreshing(false);

                            } catch (JSONException e) {
                                Log.e("error", e.toString());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null) {
                                Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                            }

                            if (error instanceof TimeoutError) {
                                Log.e("Volley", "TimeoutError " + error.toString());
                            } else if (error instanceof NoConnectionError) {
                                Log.e("Volley", "NoConnectionError");
                            } else if (error instanceof AuthFailureError) {
                                Log.e("Volley", "AuthFailureError");
                            } else if (error instanceof ServerError) {
                                Log.e("Volley", "ServerError");
                            } else if (error instanceof NetworkError) {
                                Log.e("Volley", "NetworkError");
                            } else if (error instanceof ParseError) {
                                Log.e("Volley", "ParseError");
                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", sp.getString("email", "non"));
                    return params;
                }
            };
            sr.setRetryPolicy(new DefaultRetryPolicy(
                    180000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(Home.this);
            requestQueue.add(sr);
        }


    }
    public void getOnlineStatus() {
        final ArrayList name, propic, status, email, alumni;

        name = new ArrayList();
        propic = new ArrayList();
        status = new ArrayList();
        email = new ArrayList();
        alumni = new ArrayList();

        StringRequest sr = new StringRequest(Request.Method.POST, "http://triumphit.tech/project_alumni/getOnlineStatus.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jo;
                        Log.e("error", response);
                        try {
                            jo = new JSONObject(response);
                            JSONArray jr = jo.getJSONArray("FullData");
                            //Log.e("error", jr.length() + "");
                            for (int t = 0; t < jr.length(); t++) {
                                JSONObject jsonObject = jr.getJSONObject(t);
                                name.add("" + jsonObject.getString("name"));
                                propic.add(jsonObject.getString("propic"));
                                status.add(jsonObject.getString("status"));
                                email.add(jsonObject.getString("email"));
                                alumni.add(jsonObject.getString("alumni"));

                            }

                            binding.onlineList.setAdapter(new OnlineListAdapter(Home.this, status, name, email, propic, alumni));
                            binding.swipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }

                        if (error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError " + error.toString());
                        } else if (error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", sp.getString("email", "non"));
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(Home.this);
        requestQueue.add(sr);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.home, menu);

        MenuItem notification = menu.findItem(R.id.postHomeNotification);
        //MenuItem message = menu.findItem(R.id.postHomeNotification);
        MenuItemCompat.setActionView(notification, R.layout.feeds_update_count);
        notifCount = (RelativeLayout) MenuItemCompat.getActionView(notification);
        TextView tv = (TextView) notifCount.findViewById(R.id.noti_count_text);
        if(mNotifCount > 11){
            tv.setText("11+");
        }else {
            tv.setText("" + mNotifCount);
        }
        ImageButton b = (ImageButton) notifCount.findViewById(R.id.notif_count);
        ImageButton m = (ImageButton) notifCount.findViewById(R.id.message_count);
        RelativeLayout notiTextHolder = (RelativeLayout) notifCount.findViewById(R.id.noti_text_holder);

        if(mNotifCount == 0){
            notiTextHolder.setVisibility(View.GONE);
        }

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, Notification.class));
            }
        });

        m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, Chat.class));
            }
        });
        //b.setText(String.valueOf(mNotifCount));

        return super.onCreateOptionsMenu(menu);

    }


    boolean anim = true;
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.postHomeMenu){
            if(anim){
                animatePosterVisible();
                anim = false;
                Log.e("should animante ", "shw");
            }else{
                animatePosterInvisible();
                Log.e("should animante ", "dnt");
                anim = true;
            }
        }else if(item.getItemId() == R.id.postHomeNotification){

        }
        return true;
    }

    @Override
    public void onRefresh() {
        getFeed();
        getOnlineStatus();
    }

    @Override
    public void OnReceive(int count) {
        mNotifCount = count;
        ActivityCompat.invalidateOptionsMenu(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mNotifCount = sp.getInt("noti_count", 0);
        ActivityCompat.invalidateOptionsMenu(this);
    }


}
